# Engineering meeting $
### Organisation ###
Meetings are usually conducted Thursday lunch every fortnight. Attendees should RSVP to ensure adequate supplies of pizza are ordered.

### Proposals ###

Talk proposals should be created as issues. They should include a title and a short description of the talk, as well as an indication of when they will be ready by. If no indication of when they are for is provided they may be scheduled for the next meeting. For every meeting, the three oldest submissions will be up for voting. 

Proposals should be assigned to the proposed speaker.